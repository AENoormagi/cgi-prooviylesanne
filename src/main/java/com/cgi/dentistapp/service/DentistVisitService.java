package com.cgi.dentistapp.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    public void addVisit(String dentistName, LocalDateTime visitTime) {
        DentistVisitEntity visit = new DentistVisitEntity(dentistName, visitTime);
        dentistVisitDao.create(visit);
    }

    public List<DentistVisitEntity> listVisits () {
        return dentistVisitDao.getAllVisits();
    }

    public List<DentistVisitEntity> searchVisits(String theSearchName) {
        return dentistVisitDao.searchVisits(theSearchName);
    }

    public DentistVisitEntity findById(Long theId) {
            return dentistVisitDao.findById(theId);

    }

    public void deleteById(Long theId) {
        dentistVisitDao.deleteById(theId);
    }

    public void saveVisit(DentistVisitEntity visit) {
        dentistVisitDao.saveVisit(visit);
    }
}
