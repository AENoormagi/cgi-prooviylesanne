package com.cgi.dentistapp.dao;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
    }

    public List<DentistVisitEntity> searchVisits(String theSearchName) {

        List<DentistVisitEntity> visits;

        if (theSearchName != null && theSearchName.trim().length() > 0) {
            visits = entityManager.createQuery(
                    "SELECT e FROM DentistVisitEntity e " +
                            "WHERE LOWER(e.dentistName) LIKE '%"+ theSearchName.toLowerCase() + "%' " +
                            "OR e.visitTime LIKE '%" + theSearchName + "%'").getResultList();
        } else {
            visits = entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
        }
        return visits;
    }

    public DentistVisitEntity findById(Long theId) {
        return (DentistVisitEntity) entityManager.createQuery(
                "SELECT e FROM DentistVisitEntity e WHERE e.id = " + theId).getSingleResult();
    }

    public void deleteById(Long theId) {
            entityManager.createQuery("DELETE FROM DentistVisitEntity WHERE id = " + theId).executeUpdate();
    }

    public void saveVisit(DentistVisitEntity visit) {
        Session currentSession = entityManager.unwrap(Session.class);
        currentSession.saveOrUpdate(visit);
    }
}
