package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO) {
        return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "form";
        }
        dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime());
        return "redirect:/results";
    }


    @GetMapping("/visits")
    public String showAllVisits(Model theModel) {
        List<DentistVisitEntity> theVisits = dentistVisitService.listVisits();
        theModel.addAttribute("visits", theVisits);
        return "visits";
    }

    @GetMapping("/search")
    public String searchVisits(@RequestParam("theSearchName") String theSearchName, Model theModel) {
        List<DentistVisitEntity> theVisits = dentistVisitService.searchVisits(theSearchName);
        theModel.addAttribute("visits", theVisits);
        return "visits";
    }

    @GetMapping("/detail")
    public String detail(@RequestParam("visitId") Long theId, Model theModel) {
        DentistVisitEntity visit = dentistVisitService.findById(theId);
        theModel.addAttribute("visit", visit);
        return "detail-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("visitId") Long theId) {
        dentistVisitService.deleteById(theId);
        return "redirect:/visits";
    }

    @PostMapping("/update")
    public String update(@RequestParam("visitId") Long theId, Model theModel) {
        DentistVisitEntity visit = dentistVisitService.findById(theId);
        dentistVisitService.saveVisit(visit);
        theModel.addAttribute("visit", visit);
        return "detail-form";
    }
}
