# Dentistapp

Dentistapp is for the user to make appointments to the Dentist as well as viewing, editing and deleting existing appointments.

## Installation

Use the  Gitlab package: https://gitlab.com/AENoormagi/cgi-prooviylesanne to install Dentistapp.

## Requirements

- Java 8
- Java jdk 1.8

## Usage

In terminal:

- git clone https://gitlab.com/AENoormagi/cgi-prooviylesanne.git
- cd to project location
- mvn package
- cd target
- java -jar dentistapp-0.0.1-SNAPSHOT.jar

Intellij:
- import project from project location
- Run: src.main.java.com.cgi.dentistapp.DentistAppApplication

## Contributing

Pull requests are welcome.
